package com.test.rest;

import com.test.business.Account;
import com.test.data.store.DataStore;
import com.test.data.store.impl.InMemoryDataStore;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

@Path("account")
public class AccountService {

    DataStore dataStore = InMemoryDataStore.getInstance();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get/{id}")
    public Account refillBalance(@PathParam("id") Long accountId) {
        return dataStore.getById(accountId);
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/add")
    public Long addAccount(com.test.business.Account account) {
        return dataStore.addAccount(account);
    }
}
