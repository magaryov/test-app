package com.test.rest;

import com.test.business.Transaction;
import com.test.transactions.TransactionManager;
import com.test.transactions.impl.TransactionManagerImpl;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("transfer")
public class TransferService {

    TransactionManager transactionManager = TransactionManagerImpl.getInstance();

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public Boolean transferMoney(Transaction transaction) {
        return transactionManager.transfer(transaction);
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/refill")
    public Boolean refillBalance(Transaction transaction) {
        return transactionManager.refill(transaction);
    }
}
