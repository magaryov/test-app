package com.test.rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.test.business.Account;
import com.test.business.Transaction;
import com.test.data.store.DataStore;
import com.test.data.store.impl.InMemoryDataStore;
import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class TransferServiceTest {

    private HttpServer server;
    private WebTarget target;
    private DataStore ds = InMemoryDataStore.getInstance();
    private Long id1, id2;

    @Before
    public void setUp() throws Exception {
        server = Main.startServer();
        Client c = ClientBuilder.newClient();
        target = c.target(Main.BASE_URI);

        Account account = new Account();
        account.setName("User 1");
        account.setBalance(new BigDecimal(1000));
        id1 = ds.addAccount(account);

        Account account2 = new Account();
        account2.setName("User 2");
        account2.setBalance(new BigDecimal(2000));
        id2 = ds.addAccount(account2);
    }

    @After
    public void tearDown() throws Exception {
        server.shutdownNow();
    }

    @Test
    public void testRefillBalance() {
        Transaction t = new Transaction();
        t.setToAccountId(id1);
        t.setAmount(new BigDecimal(234));

        Boolean responseMsg = target.path("transfer/refill").request().put(
                Entity.entity(t, MediaType.APPLICATION_JSON_TYPE), Boolean.class);

        assertEquals(true, responseMsg);
        assertEquals(new BigDecimal(1234), ds.getById(id1).getBalance());
    }

    @Test
    public void testTransfer() {
        Transaction t = new Transaction();
        t.setFromAccountId(id1);
        t.setToAccountId(id2);
        t.setAmount(new BigDecimal(100));
        Boolean responseMsg = target.path("transfer").request().put(
                Entity.entity(t, MediaType.APPLICATION_JSON_TYPE), Boolean.class);
        assertEquals(true, responseMsg);
        assertEquals(new BigDecimal(900), ds.getById(id1).getBalance());
        assertEquals(new BigDecimal(2100), ds.getById(id2).getBalance());
    }

    @Test
    public void testTransferMoreThanBalance() {
        Transaction t = new Transaction();
        t.setFromAccountId(id1);
        t.setToAccountId(id2);
        t.setAmount(new BigDecimal(1100));
        Boolean responseMsg = target.path("transfer").request().put(
                Entity.entity(t, MediaType.APPLICATION_JSON_TYPE), Boolean.class);
        assertEquals(false, responseMsg);
        assertEquals(new BigDecimal(1000), ds.getById(id1).getBalance());
        assertEquals(new BigDecimal(2000), ds.getById(id2).getBalance());
    }
}
