package com.test.rest;

import com.test.business.Account;
import com.test.data.store.DataStore;
import com.test.data.store.impl.InMemoryDataStore;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AccountServiceTest {

    private HttpServer server;
    private WebTarget target;
    private DataStore ds = InMemoryDataStore.getInstance();

    @Before
    public void setUp() throws Exception {
        server = Main.startServer();
        Client c = ClientBuilder.newClient();
        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.shutdownNow();
    }

    @Test
    public void testAddAccount() {
        Account account = new Account();
        account.setName("User 1");
        account.setBalance(new BigDecimal(1000));

        Long accountId = target.path("account/add").request().post(
                Entity.entity(account, MediaType.APPLICATION_JSON_TYPE), Long.class);

        assertEquals(account, ds.getById(accountId));

        Account accountReceived = target.path("account/get/" + accountId).request().get(Account.class);

        assertEquals(account, accountReceived);
    }

    @Test
    public void testGetAccount() {
        Account account = new Account();
        account.setName("User 2");
        account.setBalance(new BigDecimal(2000));

        Long accountId = target.path("account/add").request().post(
                Entity.entity(account, MediaType.APPLICATION_JSON_TYPE), Long.class);

        Account accountReceived = target.path("account/get/" + accountId).request().get(Account.class);

        assertEquals(account, accountReceived);
    }
}
