package com.test.business;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
    public ReentrantLock lock = new ReentrantLock();

    private String name;
    private volatile BigDecimal balance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass())
            return false;
        return name.equals(((Account)obj).getName()) && balance.equals(((Account)obj).getBalance());
    }
}
