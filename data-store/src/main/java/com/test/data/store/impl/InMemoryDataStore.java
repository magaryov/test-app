package com.test.data.store.impl;

import com.test.business.Account;
import com.test.data.store.DataStore;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class InMemoryDataStore implements DataStore {

    private Map<Long, Account> accounts = new ConcurrentHashMap<>();
    private AtomicLong accountIdSequence = new AtomicLong(1L);

    @Override
    public Account getById(Long key) {
        return accounts.get(key);
    }

    @Override
    public Long addAccount(Account account) {
        Long id = accountIdSequence.getAndIncrement();
        accounts.put(id, account);
        return id;
    }

    private static InMemoryDataStore INSTANCE = new InMemoryDataStore();

    public static InMemoryDataStore getInstance() {
        return INSTANCE;
    }
}
