package com.test.data.store;

import com.test.business.Account;

public interface DataStore {
    Account getById(Long id);
    Long addAccount(Account account);
}
