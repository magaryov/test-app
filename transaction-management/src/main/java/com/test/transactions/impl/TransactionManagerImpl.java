package com.test.transactions.impl;

import com.test.business.Account;
import com.test.business.InsufficientBalanceException;
import com.test.business.Transaction;
import com.test.data.store.DataStore;
import com.test.data.store.impl.InMemoryDataStore;
import com.test.transactions.TransactionManager;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class TransactionManagerImpl implements TransactionManager {

    private DataStore dataStore = InMemoryDataStore.getInstance();

    @Override
    public Boolean transfer(Transaction transaction) {
        try {
            return CompletableFuture.supplyAsync(() -> transfer0(transaction)).handle((result, exception) -> {
                if (exception != null) {
                    exception.printStackTrace();
                    return false;
                }
                else
                    return result;
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Boolean transfer0(Transaction transaction){
        Boolean isDone = false;
        Account fromAccount = dataStore.getById(transaction.getFromAccountId());
        Account toAccount = dataStore.getById(transaction.getToAccountId());

        while (!isDone) {
            try {
                if (fromAccount.lock.tryLock()) {
                    try {
                        if (toAccount.lock.tryLock()) {
                            if (fromAccount.getBalance().compareTo(transaction.getAmount()) < 0)
                                throw new InsufficientBalanceException(
                                        String.format("Cannot transfer %sUSD from [%s] to [%s]",
                                                transaction.getAmount(),
                                                transaction.getFromAccountId(),
                                                transaction.getToAccountId()));
                            fromAccount.setBalance(fromAccount.getBalance().subtract(transaction.getAmount()));
                            toAccount.setBalance(toAccount.getBalance().add(transaction.getAmount()));
                            isDone = true;
                        }
                    } finally {
                        if (toAccount.lock.isHeldByCurrentThread())
                            toAccount.lock.unlock();
                    }
                }
            } finally {
                if (fromAccount.lock.isHeldByCurrentThread())
                    fromAccount.lock.unlock();
            }
        }

        return true;
    }

    @Override
    public Boolean refill(Transaction transaction) {
        try {
            return CompletableFuture.supplyAsync(() -> refill0(transaction)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    private Boolean refill0(Transaction transaction) {
        Boolean isDone = false;
        Account toAccount = dataStore.getById(transaction.getToAccountId());

        while (!isDone) {
            try {
                if (toAccount.lock.tryLock()) {
                    toAccount.setBalance(toAccount.getBalance().add(transaction.getAmount()));
                    isDone = true;
                }
            } finally {
                if (toAccount.lock.isHeldByCurrentThread())
                    toAccount.lock.unlock();
            }
        }

        return true;
    }

    private TransactionManagerImpl() {}

    private static TransactionManagerImpl INSTANCE = new TransactionManagerImpl();

    public static TransactionManagerImpl getInstance() {
        return INSTANCE;
    }
}
