package com.test.transactions;

import com.test.business.Transaction;

public interface TransactionManager {
    Boolean transfer(Transaction transaction);
    Boolean refill(Transaction transaction);
}
